import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActorApiService } from 'src/app/actor-api.service';

@Component({
  selector: 'app-add-edit-actor',
  templateUrl: './add-edit-actor.component.html',
  styleUrls: ['./add-edit-actor.component.css']
})
export class AddEditActorComponent implements OnInit {

  actorList$!: Observable<any>[];

  constructor(private service:ActorApiService) { }

  @Input() actor:any;
  id: number = 0;
  nombre: string = '';
  fechaNacimiento: Date = new Date();
  foto: string = '';

  ngOnInit(): void {
    this.id = this.actor.id;
    this.nombre = this.actor.nombre;
    this.fechaNacimiento = this.actor.fechaNacimiento;
    this.foto = this.actor.foto;
  }

  addActor() {
    var actor = {
      nombre: this.nombre,
      fechaNacimiento: this.fechaNacimiento,
      foto: this.foto
    }
    this.service.addActor(actor).subscribe(res =>{
      var closeModalBtn = document.getElementById('add-edit-modal-close');
      if(closeModalBtn) {
        closeModalBtn.click();
      }

      var showAddSuccess = document.getElementById('add-success-alert');
      if(showAddSuccess) {
        showAddSuccess.style.display = "block";
      }
      setTimeout(function() {
        if(showAddSuccess) {
          showAddSuccess.style.display = "none"
        }
      }, 4000);
    })

  }

  addActorPrueba() {
    var actor = {
      id: 13 + 1,
      nombre: this.nombre,
      fechaNacimiento: this.fechaNacimiento,
      foto: this.foto
    }
    this.service.addActorPrueba(actor)
      var closeModalBtn = document.getElementById('add-edit-modal-close');
      if(closeModalBtn) {
        closeModalBtn.click();
      }

      var showAddSuccess = document.getElementById('add-success-alert');
      if(showAddSuccess) {
        showAddSuccess.style.display = "block";
      }
      setTimeout(function() {
        if(showAddSuccess) {
          showAddSuccess.style.display = "none"
        }
      }, 4000);
  }

  updateActor() {
    var actor = {
      id: this.id,
      nombre: this.nombre,
      fechaNacimiento: this.fechaNacimiento,
      foto: this.foto
    }
    var id:number = this.id;
    this.service.updateActor(id,actor).subscribe(res =>{
      var closeModalBtn = document.getElementById('add-edit-modal-close');
      if(closeModalBtn) {
        closeModalBtn.click();
      }

      var showUpdateSuccess = document.getElementById('update-success-alert');
      if(showUpdateSuccess) {
        showUpdateSuccess.style.display = "block";
      }
      setTimeout(function() {
        if(showUpdateSuccess) {
          showUpdateSuccess.style.display = "none"
        }
      }, 4000);
    })
  }

}
