import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActorApiService } from 'src/app/actor-api.service';

@Component({
  selector: 'app-get-actor',
  templateUrl: './get-actor.component.html',
  styleUrls: ['./get-actor.component.css']
})
export class GetActorComponent implements OnInit {

  actorList$!:Observable<any[]>;
  autoresPrueba = [{"id": 0, "nombre": "", "fechaNacimiento": "", "foto": ""}];

  constructor(private service:ActorApiService) { }

  ngOnInit(): void {
    //this.actorList$ = this.service.getActorList();
    this.autoresPrueba = this.service.getActorListPrueba()
  }

  modalTitle:string = '';
  activateAddEditActorComponent:boolean = false;
  actor:any;

  modalAdd() {
    this.actor = {
      id: 0,
      nombre: null,
      fechaNacimiento: new Date(),
      foto: null
    }
    this.modalTitle = "Agregar Actor";
    this.activateAddEditActorComponent = true;
  }

  modalEdit(item:any) {
    this.actor = item;
    this.modalTitle = "Editar Actor";
    this.activateAddEditActorComponent = true;
  }

  delete(item:any) {
    if(confirm(`Esta seguro de borrar ${item.nombre}`)) {
      this.service.deleteActor(item.id).subscribe(res => {
        var closeModalBtn = document.getElementById('add-edit-modal-close');
        if(closeModalBtn) {
          closeModalBtn.click();
        }
  
        var showDeleteSuccess = document.getElementById('delete-success-alert');
        if(showDeleteSuccess) {
          showDeleteSuccess.style.display = "block";
        }
        setTimeout(function() {
          if(showDeleteSuccess) {
            showDeleteSuccess.style.display = "none"
          }
        }, 4000);
        //this.actorList$ = this.service.getActorList();
      })
    }
  }


  modalClose() {
    this.activateAddEditActorComponent = false;
    //this.actorList$ = this.service.getActorList();
  }

}
