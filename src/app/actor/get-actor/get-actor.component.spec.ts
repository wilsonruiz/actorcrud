import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetActorComponent } from './get-actor.component';

describe('GetActorComponent', () => {
  let component: GetActorComponent;
  let fixture: ComponentFixture<GetActorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetActorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetActorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
