import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ActorComponent } from './actor/actor.component';
import { GetActorComponent } from './actor/get-actor/get-actor.component';
import { AddEditActorComponent } from './actor/add-edit-actor/add-edit-actor.component';
import { ActorApiService } from './actor-api.service';

@NgModule({
  declarations: [
    AppComponent,
    ActorComponent,
    GetActorComponent,
    AddEditActorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ActorApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
