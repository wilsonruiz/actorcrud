import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import dataActores from './actores.json';

@Injectable({
  providedIn: 'root'
})
export class ActorApiService {
 
  readonly peliculaAPIUrl = "https://localhost:7036/api";

  constructor(private http:HttpClient) { }

  getActorList():Observable<any> {
    return this.http.get<any>(this.peliculaAPIUrl + '/actores');
  }

    //Datos Prueba
  getActorListPrueba() {
    return dataActores;
  }

  addActor(data:any) {
    return this.http.post(this.peliculaAPIUrl + '/actores', data);
  }
      //Datos prueba
  addActorPrueba(data:any) {
    return dataActores.push(data);
  }

  updateActor(id:number|string, data:any) {
    return this.http.put(this.peliculaAPIUrl + `/actores/${id}`, data);
  }


  deleteActor(id:number|string) {
    return this.http.delete(this.peliculaAPIUrl + `/actores/${id}`);
  }

}
